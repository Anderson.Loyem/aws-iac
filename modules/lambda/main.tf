
resource "aws_lambda_function" "lambda" {
  filename      = "${path.module}/lambda_function.py.zip"
  function_name = var.name
  role          = var.role
  handler       = var.handler
  runtime       = var.runtime
  tags          = var.tags
  timeout       = var.timeout
}
