/*


*/

resource "aws_api_gateway_api_key" "assurly_web_app" {
  for_each = {
    for index, vm in var.api_key_list :
    index => vm
  }
  name        = each.value.Name
  description = " "
  value       = each.value.Value
  tags        = var.aws_common_tag
}

resource "aws_api_gateway_usage_plan" "my_usage_plan" {
  name        = "Basic"
  description = ""
  api_stages {
    api_id = aws_api_gateway_rest_api.api.id
    stage  = aws_api_gateway_deployment.assurly_deploy.stage_name
  }
  quota_settings {
    limit  = 20000
    offset = 2
    period = "WEEK"
  }

  throttle_settings {
    burst_limit = 5
    rate_limit  = 10
  }
  tags = var.aws_common_tag
}

resource "aws_api_gateway_usage_plan_key" "main" {
  for_each = {
    for index, vm in aws_api_gateway_api_key.assurly_web_app :
    index => vm
  }
  key_type      = "API_KEY"
  key_id        = each.value.id
  usage_plan_id = aws_api_gateway_usage_plan.my_usage_plan.id
}