variable "env" {
  description = "env: dev or prod"
  default = "dev"
}

variable "aws_common_tag" {
  type        = map(any)
  description = "aws tag commun"
}


variable "api_key_list" {
  type        = list(map(any))
  description = "Liste de api key d'assurly et partenaire"
  default = [{
    Name  = "assurly",
    Value = "assurly-b539f812-bee0-4068-8ab9-c46d9b03521a"
    },
    {
      Name  = "avostart",
      Value = "avostart-289d-43c7-8ff4-32f9d323be1f"
    },
    {
      Name  = "cautioneo",
      Value = "cautioneo-3c1e2774-281d-4d43-a189-db7cf2a6d372"
    },
    {
      Name  = "qlower",
      Value = "qlower-4f6165af-306f-444b-b744-227cb83d149f"
    },
    {
      Name  = "rakuten",
      Value = "rakuten-3a59-4f06-9541-f5a780835e17"
    },
    {
      Name  = "webapp",
      Value = "webapp-b539f812-bee0-4068-8ab9-c46d9b03521a"
    },
  ]
}
