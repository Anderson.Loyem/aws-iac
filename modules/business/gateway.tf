
# API Gateway

resource "aws_api_gateway_rest_api" "api" {
  name = "assurly"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
  description = "endpoint des api consommé par le mobile et webapp"
  tags        = var.aws_common_tag
}

# method

resource "aws_api_gateway_resource" "resource" {
  path_part   = "insuree"
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.api.id
}

module "cors" {
  source  = "squidfunk/api-gateway-enable-cors/aws"
  version = "0.3.3"

  api_id          = aws_api_gateway_rest_api.api.id
  api_resource_id = aws_api_gateway_resource.resource.id
  allow_credentials = true
  allow_headers   = ["Authorization", "Content-Type", "X-Amz-Date", "X-Amz-Security-Token", "X-Api-Key"]
  allow_origin    = "*"
}

/*
resource "aws_api_gateway_resource" "resource2" {
  path_part   = "email"
  parent_id   = aws_api_gateway_resource.resource.rest_api_id 
  #aws_api_gateway_rest_api.api.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.api.id
}
*/

/*** deploy endpoint ***/

resource "aws_api_gateway_deployment" "assurly_deploy" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  stage_name  = "v0"
}
