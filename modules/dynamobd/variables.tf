variable "env" {
  type        = string
  description = "(optional) describe your variable"
}

variable "name_table" {
  type        = string
  description = "(optional) describe your variable"
}

variable "hash_key" {
  type        = string
  description = "(optional) describe your variable"
}

variable "range_key" {
  type        = string
  description = "(optional) describe your variable"
}

variable "billing_mode" {
  type        = string
  description = "(optional) describe your variable"
  default     = "PROVISIONED"
}

variable "tags" {
  type        = map(any)
  description = "aws tag commun"
  default = {
    Name = "assurly-back"
  }
}

variable "attribute_list" {
  type        = list(object({ name = string, type = string }))
  description = ""
  default     = []
}

variable "index_list" {
  type        = list(object({ name = string, hash_key = string, non_key = list(string) }))
  description = ""
  default     = []
}
